% working bad 1
clear
clc
addpath('./casadi-matlabR2014b-v3.2.3');
import casadi.*


T = 5; % Time horizon
N = 100; % number of control intervals

% needed characteristics
x_needed = 13;
y_needed = 0;
alpha_needed = 0;

% initial position
x_initial = 0;
y_initial = 0;
alpha_initial = 0;

% initial velocity
xdot_initial = 0;
ydot_initial = 0;
alphadot_initial = 0;



% system constants
m=0.6;
gravity = 9.8;
I = 16.17e-3;
b = 0.285;

% Declare model variables
x1 = SX.sym('x1'); % stands for x
x2 = SX.sym('x2'); % stands for y
x3 = SX.sym('x3'); % stands for alpha
x4 = SX.sym('x4'); % stands for Vx
x5 = SX.sym('x5'); % stands for Vy
x6 = SX.sym('x6'); % stands for Valpha
%x = [x1; x2];
x = [x1; x2; x3; x4; x5; x6];
u1 = SX.sym('u1');
u2 = SX.sym('u2');
u = [ u1; u2];

% Model equations
%xdot = [(1-x2^2)*x1 - x2 + u; x1];
xdot = [x4;
        x5;
        x6;
        -1/m*(u1+u2)*sin(x3);
        1/m*(u1+u2)*cos(x3)-gravity;
        1/I*(u2-u1)*b];

% Objective term
%L = x1^2 + x2^2 + u^2;
C1 = 1;
for_goal = [x1 - x_needed ; x2 - y_needed ; x3 - alpha_needed];
L = for_goal'*C1*for_goal;


% Continuous time dynamics
f = Function('f', {x, u}, {xdot, L});

% Formulate discrete time dynamics
if 1
   % CVODES from the SUNDIALS suite
   dae = struct('x',x,'p',u,'ode',xdot,'quad',L);
   opts = struct('tf',T/N);
   F = integrator('F', 'cvodes', dae, opts);
else
   % Fixed step Runge-Kutta 4 integrator
   M = 4; % RK4 steps per interval
   DT = T/N/M;
   f = Function('f', {x, u}, {xdot, L});
   X0 = MX.sym('X0', 2);
   U = MX.sym('U');
   X = X0;
   Q = 0;
   for j=1:M
       [k1, k1_q] = f(X, U);
       [k2, k2_q] = f(X + DT/2 * k1, U);
       [k3, k3_q] = f(X + DT/2 * k2, U);
       [k4, k4_q] = f(X + DT * k3, U);
       X=X+DT/6*(k1 +2*k2 +2*k3 +k4);
       Q = Q + DT/6*(k1_q + 2*k2_q + 2*k3_q + k4_q);
    end
    F = Function('F', {X0, U}, {X, Q}, {'x0','p'}, {'xf', 'qf'});
end

% Evaluate at a test point
Fk = F('x0',[0; 0; 0; 0; 0; 0;],'p',[2.98 ; 2.94]);
disp(Fk.xf)
disp(Fk.qf)

% % Start with an empty NLP
w={}; % 
w0 = [];
lbw = [];
ubw = [];
J = 0;
g={};
lbg = [];
ubg = [];

% "Lift" initial conditions
Xk = MX.sym('X0', 6);
w = {w{:}, Xk};
% initial state?
lbw = [lbw; x_initial; y_initial; alpha_initial; xdot_initial; ydot_initial; alphadot_initial];
ubw = [ubw; x_initial; y_initial; alpha_initial; xdot_initial; ydot_initial; alphadot_initial];
w0 = [w0; x_initial; y_initial; alpha_initial; xdot_initial; ydot_initial; alphadot_initial];

% Formulate the NLP
for k=0:N-1
    % New NLP variable for the control
    Uk = MX.sym(['U_' num2str(k)],2);
    w = {w{:}, Uk};
    lbw = [lbw; 0; 0];
    ubw = [ubw;  10; 10];
    w0 = [w0;  2.94; 2.94];

    % Integrate till the end of the interval
    Fk = F('x0', Xk, 'p', Uk);
    Xk_end = Fk.xf;
    J=J+Fk.qf;

    % New NLP variable for state at end of interval
    Xk = MX.sym(['X_' num2str(k+1)], 6);
    w = [w, {Xk}];
    lbw = [lbw; -inf; -inf; -inf; -inf; -inf; -2*pi];
    ubw = [ubw;  inf;  inf;  inf;  inf;  inf;  2*pi];
    
    coeff = k/(N-1);
    
    w0 = [w0;
        x_initial + coeff*( x_needed - x_initial);
        y_initial + coeff*( y_needed - y_initial);
        alpha_initial + coeff*( alpha_needed - alpha_initial);
        0;
        0;
        0;];

    % Add equality constraint
    g = [g, {Xk_end-Xk}];
    lbg = [lbg; 0; 0; 0; 0; 0; 0];
    ubg = [ubg; 0; 0; 0; 0; 0; 0];
end
% 
% Create an NLP solver
prob = struct('f', J, 'x', vertcat(w{:}), 'g', vertcat(g{:}));
solver = nlpsol('solver', 'ipopt', prob);

% Solve the NLP
sol = solver('x0', w0, 'lbx', lbw, 'ubx', ubw,...
            'lbg', lbg, 'ubg', ubg);
w_opt = full(sol.x);

% Plot the solution
x_opt =     w_opt(1:8:end);
y_opt =     w_opt(2:8:end);
alpha_opt = w_opt(3:8:end);
vx_opt = w_opt(4:8:end);
vy_opt = w_opt(5:8:end);
valpha_opt = w_opt(6:8:end);
u1 = w_opt(7:8:end);
u2 = w_opt(8:8:end);
tgrid = linspace(0, T, N+1);
clf;
hold on
plot(tgrid, x_opt, '--')
plot(tgrid, y_opt, '-')
plot(tgrid, alpha_opt, '-.')
%stairs(tgrid, [alpha_opt; nan], '-.')
xlabel('t')
legend('x','y','alpha')
