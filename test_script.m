% plotting results of quad NLP solver

% Here we are given with total time T
% tgrid - time grid
% x_opt, y_opt, alpha_opt - optimal trajectories

LLL = length(tgrid);
pause_time = T/LLL;

fig2 = figure();
hold on

axis([-0.5 (x_needed+1) -0.5 (y_needed+1)]);
grid on
h = waitbar(0,'Simulation ...');
%set(fig2,'Position',[1921 37 1280 947]);
for i = 1: LLL
    waitbar(i/LLL);
    plot(x_opt,y_opt);
    grid on
    %axis([-0.5 (x_needed+1) -0.5 (y_needed+1)]);
    %axis([x_opt(i)-1 x_opt(i)+1  y_opt(i)-1  y_opt(i)+1]);
    axis([x_opt(i)-3 x_opt(i)+3  y_opt(i)-3  y_opt(i)+3]);
    axis equal
    hold on
    draw_2Dquadcopter(x_opt(i), y_opt(i), alpha_opt(i),0);
    pause(pause_time);
    
    hold off
    
end
close(h);
% draw_2Dquadcopter(1,1,pi/3,0);
% axis([0 2 0 2]);
% grid on