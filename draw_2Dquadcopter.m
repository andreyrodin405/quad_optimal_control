function A = draw_2Dquadcopter(x,y,alpha,rotstate)
axis equal
L = 0.285;
hld = ishold(); % saving hold station

% drawing framework
red =   231/255;
green = 186/255;
blue =  95/255;

rede =   182/255;
greene = 109/255;
bluee =  29/255;
h_frame = rectangle('Position',[-L,-0.01,2*L,0.02],'FaceColor',[red  green  blue],'EdgeColor',[rede  greene  bluee],...
    'LineWidth',1);
hold on

% drawing propellers

red =   180/255;
green = 190/255;
blue =  203/255;

rede =   112/255;
greene = 133/255;
bluee =  156/255;
h_left_mot = rectangle('Position',[-L-0.015,-0.03,0.03,0.06],'FaceColor',[red  green  blue],'EdgeColor',[rede  greene bluee],...
    'LineWidth',1);
h_right_mot = rectangle('Position',[L-0.015,-0.03,0.03,0.06],'FaceColor',[red  green  blue],'EdgeColor',[rede  greene bluee],...
    'LineWidth',1);

% drawing proprellers
 radius = 0.16;

v = [-L 0.032; (-L-radius) 0.035; -L 0.04 ; ...
    -L 0.032; (-L+radius) 0.035; -L 0.04;...
    L 0.032; (L-radius) 0.035; L 0.04;...
    L 0.032; (L+radius) 0.035; L 0.04;...
    (L-0.12*radius) 0.032; (L+0.12*radius) 0.032; L 0.4*radius;...
    (-L-0.12*radius) 0.032; (-L+0.12*radius) 0.032; -L 0.4*radius;...
    ];
f = [1 2 3; 4 5 6; 7 8 9; 10 11 12; 13 14 15; 16 17 18];
red =   11/255;
green = 11/255;
blue =  11/255;
col = [red green blue; red green blue; red green blue; red green blue; red green blue; red green blue];
h_prop = patch('Faces',f,'Vertices',v,'FaceVertexCData',col,'FaceColor','flat');

% drawing controller
red =   15/255;
green = 104/255;
blue =  202/255;

rede =   0/255;
greene = 90/255;
bluee =  200/255;
h_contr = rectangle('Position',[-0.07,-0.04,0.14,0.10],'FaceColor',[red  green  blue],'EdgeColor',[rede  greene bluee],...
    'LineWidth',2,'curvature',0.6);

% plot centerlines
h_line1 = plot([-0.07 0.07],[ 0 0],'y-','LineWidth',1);
h_line2 = plot([0 0 ],[-0.03 0.05],'y-','LineWidth',1)

% perform transformation
t = hgtransform('Parent',gca);

set(h_prop,'Parent',t);
set(h_frame,'Parent',t);
set(h_left_mot,'Parent',t);
set(h_right_mot,'Parent',t);
set(h_contr,'Parent',t);
set(h_line1,'Parent',t);
set(h_line2,'Parent',t);
Txy = makehgtform('translate',[x y 0],'zrotate',alpha);     % define a transform matrix
set(t,'Matrix',Txy);


if(~hld)
    hold off
end    
end